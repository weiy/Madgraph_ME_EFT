import os
import sys

os.environ['PYTHONPATH'] = "/afs/cern.ch/user/w/weiy/.local/lib/python2.7/site-packages" + os.environ['PYTHONPATH']

from skhep.math import LorentzVector
#import subprocess
import allmatrix2py
import numpy as np
#from ROOT import TFile, TTree, TLorentzVector, vector
from ROOT import *
#import os
#import shutil
from datetime import datetime
start=datetime.now()

#setupATLAS
#lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"
#os.environ['LD_LIBRARY_PATH'] = "/scratch/rcoelhol/MadGraph/MG5_aMC_v2_7_2/HEPTools/lhapdf6/lib"+os.environ['LD_LIBRARY_PATH']
#os.environ['PYTHONPATH'] = "/scratch/rcoelhol/MadGraph/MG5_aMC_v2_7_2/HEPTools/lhapdf6/lib/python2.7/site-packages/" + os.environ['PYTHONPATH']

# some local stuff
#os.environ['PATH'] = "/home/net3/rcoelhol/.local/bin" + os.environ['PATH']
#os.environ['PYTHONPATH'] = "/home/net3/rcoelhol/.local/lib/python2.7/site-packages" + os.environ['PYTHONPATH']

def invert_momenta(p):
        """ fortran/C-python do not order table in the same order"""
        new_p = []
        for i in range(len(p[0])):  new_p.append([0]*len(p))
        for i, onep in enumerate(p):
            for j, x in enumerate(onep):
                new_p[j][i] = x
        return new_p

allmatrix2py.initialise('./param_card.dat')

def Calculate_ME(inputdir, outputdir, filename, EFT_op):

    lep1Z1 = TLorentzVector()
    lep2Z1 = TLorentzVector()
    lep1Z2 = TLorentzVector()
    lep2Z2 = TLorentzVector()
    
    gluon1 = TLorentzVector()
    gluon2 = TLorentzVector()
    
    myfile = TFile(inputdir+'/'+filename)

    #output tree with new branches
    #path = pathToFile.replace('/home/weiy/transfer_server/miniTrees_EFT_4l_jay/OpgSquared','')
    #output_tree = '../'+'Added_Bkg_ME/minitrees_EFT/'+path
    output_tree = outputdir+ '/'+filename.replace('.root','') + '_ME_' + EFT_op + '.root'
    f = TFile(output_tree,'RECREATE')

    #get the minitree from the TFile and loop over the entries
    mytree = myfile.Get('tree_incl_all')
    entries = mytree.GetEntriesFast()

    #lepton_pt = vector('float')()
    #lepton_eta = vector('float')()
    #lepton_phi = vector('float')()
    #lepton_m = vector('float')()

    #mytree.SetBranchAddress("lepton_pt_fidDres_truth", lepton_pt);
    #mytree.SetBranchAddress("lepton_eta_fidDres_truth", lepton_eta);
    #mytree.SetBranchAddress("lepton_phi_fidDres_truth", lepton_phi);
    #mytree.SetBranchAddress("lepton_m_fidDres_truth", lepton_m);
    
    #defining all the required variables for new branches
    #VBF_Sig_ME = np.array([-99.0])
    ME_SMEFT_gg4l_address_truth = np.array([-999.0])
    ME_SMEFT_gg4l_address_4lsel = np.array([-999.0])
    m4l_fsr_address = np.array([-999.0])
    #qqZZ_2j_ME = np.array([-99.0])

    #cloning the old tree to get a new tree: only clone several branches for checking later to reduce the output file
    #mytree.SetBranchStatus("*", 0)
    #mytree.SetBranchStatus("m4l_fsr",1)
    #mytree.SetBranchStatus("lepton_pt",1)
    #mytree.SetBranchStatus("lepton_eta",1)
    #mytree.SetBranchStatus("lepton_phi",1)
    #mytree.SetBranchStatus("lepton_m",1)
    #mytree.SetBranchStatus("event_type",1)
    #mytree.SetBranchStatus("weight",1)

    #newTree = mytree.CloneTree(0)
    #tree = TTree( 'T', 'staff data from ascii file' )
    newTree = TTree("tree_incl_all", "tree_incl_all")
    #adding all the new ME branches
    #newTree.Branch("VBF_Sig_ME", VBF_Sig_ME, 'VBF_Sig_ME/D')
    #newTree.Branch("ME_SMEFT_gg4l_OpgSquared_weiy", ME_SMEFT_gg4l_OtpSM, 'ME_SMEFT_gg4l_OtpSquared_weiy/D')
    newTree.Branch("ME_SMEFT_gg4l_"+EFT_op+"_fidDres_truth", ME_SMEFT_gg4l_address_truth, "ME_SMEFT_gg4l_"+EFT_op+"_fidDres_truth/D")
    newTree.Branch("ME_SMEFT_gg4l_"+EFT_op+"_fidDres_4lsel", ME_SMEFT_gg4l_address_4lsel, "ME_SMEFT_gg4l_"+EFT_op+"_fidDres_4lsel/D")

    newTree.Branch("higgs_m_fidDres_truth", m4l_fsr_address, "higgs_m_fidDres_truth/D")
    #newTree.Branch("qqZZ_2j_ME", qqZZ_2j_ME, 'qqZZ_2j_ME/D')

    #TBranch* Branch(const char* name, void* address, const char* leaflist)
    
    total = mytree.GetEntries()
    print('total number of events is: '+str(total))
    scale = 1

    for jentry in range(0, total):
        ientry = mytree.LoadTree(jentry)
        if ientry < 0:
            break
        nb = mytree.GetEntry(jentry)
        if nb<=0:
            continue

        #VBF_sig_ME_value = -999
        ME_gg4l_value = -999
        #qqZZ_2j_ME_value = -999

        m4l_fsr_address[0] = mytree.higgs_m_fidDres_truth
        
        if mytree.higgs_m_fidDres_truth>130.0 and mytree.higgs_m_fidDres_truth<2000:
		if mytree.weight!=0.0:
                    lep1Z1.SetPtEtaPhiM(mytree.lepton_pt_fidDres_truth[0], mytree.lepton_eta_fidDres_truth[0], mytree.lepton_phi_fidDres_truth[0], mytree.lepton_m_fidDres_truth[0])
                    lep2Z1.SetPtEtaPhiM(mytree.lepton_pt_fidDres_truth[1], mytree.lepton_eta_fidDres_truth[1], mytree.lepton_phi_fidDres_truth[1], mytree.lepton_m_fidDres_truth[1])
                    lep1Z2.SetPtEtaPhiM(mytree.lepton_pt_fidDres_truth[2], mytree.lepton_eta_fidDres_truth[2], mytree.lepton_phi_fidDres_truth[2], mytree.lepton_m_fidDres_truth[2])
                    lep2Z2.SetPtEtaPhiM(mytree.lepton_pt_fidDres_truth[3], mytree.lepton_eta_fidDres_truth[3], mytree.lepton_phi_fidDres_truth[3], mytree.lepton_m_fidDres_truth[3])
                    
                    vecZ1= (lep1Z1 + lep2Z1)
                    vecZ2= (lep1Z2 + lep2Z2)
                    
                    vecHiggs = vecZ1 + vecZ2 
                    
                    gluon1.SetPxPyPzE(0,0,vecHiggs.M()/2, vecHiggs.M()/2)
                    gluon2.SetPxPyPzE(0,0,-vecHiggs.M()/2, vecHiggs.M()/2)
                    
                    lep1Z1.Boost(-vecHiggs.BoostVector())
                    lep1Z2.Boost(-vecHiggs.BoostVector())
                    lep2Z1.Boost(-vecHiggs.BoostVector())
                    lep2Z2.Boost(-vecHiggs.BoostVector())
                    
		    if mytree.event_type_fidDres_truth==2.0:
			pboost = [[vecHiggs.M()/2., 0., 0., vecHiggs.M()/2.],
                              [vecHiggs.M()/2., 0., 0., -vecHiggs.M()/2.],
                              [lep1Z2.E(), lep1Z2.Px(), lep1Z2.Py(), lep1Z2.Pz()],
                              [lep2Z2.E(), lep2Z2.Px(), lep2Z2.Py(), lep2Z2.Pz()],
                              [lep1Z1.E(), lep1Z1.Px(), lep1Z1.Py(), lep1Z1.Pz()],
                              [lep2Z1.E(), lep2Z1.Px(), lep2Z1.Py(), lep2Z1.Pz()]]
		    else:
                    	pboost = [[vecHiggs.M()/2., 0., 0., vecHiggs.M()/2.],
                              [vecHiggs.M()/2., 0., 0., -vecHiggs.M()/2.],
                              [lep1Z1.E(), lep1Z1.Px(), lep1Z1.Py(), lep1Z1.Pz()],
                              [lep2Z1.E(), lep2Z1.Px(), lep2Z1.Py(), lep2Z1.Pz()],
                              [lep1Z2.E(), lep1Z2.Px(), lep1Z2.Py(), lep1Z2.Pz()],
                              [lep2Z2.E(), lep2Z2.Px(), lep2Z2.Py(), lep2Z2.Pz()]]
 
                    mom =invert_momenta(pboost)
                    alphas = 0.118
                    nhel = -1 # means sum over all helicity
                    proc_id = -1 # if you use the syntax "@X" (with X>0), you can set proc_id to that value (this allows to distinguish process with identical initial/final state.) 
                    
                    scale2 = 0.0
                    
		    if mytree.event_type_fidDres_truth==0.0:
			pdgs = [21,21,-13,13,-13,13]
		    elif mytree.event_type_fidDres_truth==1.0:
			pdgs = [21,21,-11,11,-11,11]
                    elif mytree.event_type_fidDres_truth==2.0 or mytree.event_type_fidDres_truth==3.0:
                        pdgs = [21,21,-11,11,-13,13]
                
                    try:
                        #print("in try")
                        if mytree.event_type_fidDres_truth <= 4.0:
                                me2 = allmatrix2py.smatrixhel(pdgs,proc_id,mom,alphas,scale2,nhel)
                        else:
                                me2 = -999
                        #print("me2 done")
                    except:
                        #print('without me2 for entry: ' + str(jentry))
                        me2 = -999.0    
                        pass

                    ME_gg4l_value = me2

        #print(ME_gg4l_value)
        try:
                #print ME_gg4l_value[0]
                ME_SMEFT_gg4l_address_truth[0] = ME_gg4l_value[0]
                if mytree.pass_fidDres_cut:
                        ME_SMEFT_gg4l_address_4lsel[0] = ME_gg4l_value[0]
                else:
                        ME_SMEFT_gg4l_address_4lsel[0] = -999

        except: 
                ME_SMEFT_gg4l_address_truth[0] = ME_gg4l_value
                if mytree.pass_fidDres_cut:
                        ME_SMEFT_gg4l_address_4lsel[0] = ME_gg4l_value
                else:
                        ME_SMEFT_gg4l_address_4lsel[0] = -999

        if jentry < 10:
                print ME_gg4l_value

        if jentry%1000==11:
                print ''
                print(str(1.0*jentry/total*100) + '% finished')
                #print("reached "+str(jentry)+"th event in sample "+str(inputdir))
                print ME_gg4l_value
                print (datetime.now()-start)
        newTree.Fill()

    f.Write()
    f.Close()
    print ''
    print '100% finished'
    print (datetime.now()-start)

#Calculate_ME("../minitrees_EFT/mc16a/Nominal/mc16_13TeV.507656.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OtpSM_OpgSM.root")
#Calculate_ME("../mc16d/Nominal/mc16_13TeV.507656.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OtpSM_OpgSM.root")
#Calculate_ME("../mc16e/Nominal/mc16_13TeV.507656.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OtpSM_OpgSM.root")
#minitree_PATH='/home/weiy/transfer_server/miniTrees_EFT_4l_jay/OpgSquared/mc16a/Nominal/'
#minitree_PATH='/home/weiy/transfer_server/AntiKt4EMPFlow/mc16a/Nominal/'

#Calculate_ME('/home/weiy/transfer_server/AntiKt4EMPFlow/mc16a/Nominal/',"mc16_13TeV.507646.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OpgSquared_OpgOtp.root")

#Calculate_ME('/home/weiy/transfer_server/Prod_v25/mc16a/Nominal/',"qqZZ.root")

#Calculate_ME('/home/weiy/transfer_server/Prod_v25/mc16a/Nominal/',"mc16_13TeV.345706.Sherpa_222_NNPDF30NNLO_ggllll_130M4l.root")

#Calculate_ME('/home/weiy/transfer_server/Prod_v25/mc16a/Nominal/',"mc16_13TeV.345706.Sherpa_222_NNPDF30NNLO_ggllll_130M4l.root")

op_name = str(sys.argv[1]) #OpgSquared
mc_campaign = str(sys.argv[2]) #mc16a
truth_or_reco = str(sys.argv[3]) #truth
sample_name = str(sys.argv[4]) #mc16_13TeV.507646.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OpgSquared_OpgOtp.root
#/data/atlas/atlasdata/weiy/BACKUP/HZZ/SMEFT/minitrees/SMEFT/reco/AntiKt4EMPFlow/mc16a/Nominal/

inputdir = ''
outputdir = ''
print(truth_or_reco)

if truth_or_reco == 'reco':
        inputdir = '/eos/user/w/weiy/ME/minitrees/reco/AntiKt4EMPFlow/'+mc_campaign+'/Nominal/split/'
        outputdir = '/eos/user/w/weiy/ME/minitrees/reco/ME_test/'+mc_campaign+'/Nominal/split/'

elif truth_or_reco == 'truth':
        inputdir = '/eos/user/w/weiy/ME/minitrees/truth/AntiKt4EMPFlow/'+mc_campaign+'/Nominal/split/'
        outputdir = '/eos/user/w/weiy/ME/minitrees/truth/ME/'+mc_campaign+'/Nominal/split/'
print(inputdir)
print(outputdir)

Calculate_ME(inputdir, outputdir, sample_name, op_name)
