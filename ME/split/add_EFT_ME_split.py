# Use python2

# Include personal python packages: skhep and etc.
os.environ['PYTHONPATH'] += "/afs/cern.ch/user/w/weiy/.local/lib/python2.7/site-packages"

import os
import sys
from skhep.math import LorentzVector
import allmatrix2py
import numpy as np
from ROOT import TFile, TTree, TLorentzVector, vector
from datetime import datetime
start=datetime.now()

# Invert momenta as fortran/C-python do not order table in the same order.
def invert_momenta(p):
    new_p = []
    for i in range(len(p[0])):
        new_p.append([0]*len(p))
    for i, onep in enumerate(p):
        for j, x in enumerate(onep):
            new_p[j][i] = x
    return new_p

# Initalizing EFT parameter card.
# Please change the card before run this script.
allmatrix2py.initialise('./param_card.dat')

# Calculate Matrix Element.
def Calculate_ME(inputdir, outputdir, filename, EFT_op):

    lep1Z1 = TLorentzVector()
    lep2Z1 = TLorentzVector()
    lep1Z2 = TLorentzVector()
    lep2Z2 = TLorentzVector()

    gluon1 = TLorentzVector()
    gluon2 = TLorentzVector()

    input_file  = TFile(inputdir+'/'+filename)
    output_tree = outputdir+ '/'+filename.replace('.root','') + '_ME_' + EFT_op + '.root'
    f = TFile(output_tree,'RECREATE')

    # Get the minitree from the TFile and loop over the entries.
    mytree  = input_file.Get('tree_incl_all')
    entries = mytree.GetEntriesFast()

    lepton_pt  = vector('float')()
    lepton_eta = vector('float')()
    lepton_phi = vector('float')()
    lepton_m   = vector('float')()

    # Final four-lepton information as the inputs of the matrix element calculation.
    mytree.SetBranchAddress("lepton_pt",  lepton_pt);
    mytree.SetBranchAddress("lepton_eta", lepton_eta);
    mytree.SetBranchAddress("lepton_phi", lepton_phi);
    mytree.SetBranchAddress("lepton_m",   lepton_m);

    # Defining all the required variables for new branches.
    ME_SMEFT_gg4l_address = np.array([-999.0])
    m4l_fsr_address = np.array([-999.0])

    newTree = TTree("tree_incl_all", "tree_incl_all")
    newTree.Branch("ME_SMEFT_gg4l_"+EFT_op, ME_SMEFT_gg4l_address, "ME_SMEFT_gg4l_"+EFT_op+"/D")
    newTree.Branch("m4l_fsr", m4l_fsr_address, "m4l_fsr/D")

    total = mytree.GetEntries()
    print('The total number of events is: '+str(total))

    # Loop over events and calculate the matrix element of each event.
    for jentry in range(0, total):
        ientry = mytree.LoadTree(jentry)
        if ientry < 0:
            break
        nb = mytree.GetEntry(jentry)
        if nb <= 0:
            continue

        ME_gg4l_value = -999
        m4l_fsr_address[0] = mytree.m4l_fsr

        # Mass window for Higgs offshell analysis.
        if mytree.m4l_fsr > 130.0 and mytree.m4l_fsr < 2000:
	    if mytree.weight != 0.0:
                
                # Outgoing four leptons.
                lep1Z1.SetPtEtaPhiM(mytree.lepton_pt[0], mytree.lepton_eta[0], mytree.lepton_phi[0], mytree.lepton_m[0])
                lep2Z1.SetPtEtaPhiM(mytree.lepton_pt[1], mytree.lepton_eta[1], mytree.lepton_phi[1], mytree.lepton_m[1])
                lep1Z2.SetPtEtaPhiM(mytree.lepton_pt[2], mytree.lepton_eta[2], mytree.lepton_phi[2], mytree.lepton_m[2])
                lep2Z2.SetPtEtaPhiM(mytree.lepton_pt[3], mytree.lepton_eta[3], mytree.lepton_phi[3], mytree.lepton_m[3])

                # Two Z bosons.
                vecZ1= (lep1Z1 + lep2Z1)
                vecZ2= (lep1Z2 + lep2Z2)

                # Higgs.
                vecHiggs = vecZ1 + vecZ2

                # Incoming two gluons in the rest framework of the Higgs particle.
                gluon1.SetPxPyPzE(0,0,vecHiggs.M()/2, vecHiggs.M()/2)
                gluon2.SetPxPyPzE(0,0,-vecHiggs.M()/2, vecHiggs.M()/2)

                # Set outgoing four leptons in the rest framework of the Higgs particle.
                lep1Z1.Boost(-vecHiggs.BoostVector())
                lep1Z2.Boost(-vecHiggs.BoostVector())
                lep2Z1.Boost(-vecHiggs.BoostVector())
                lep2Z2.Boost(-vecHiggs.BoostVector())

                # Matrix element calculation requires full info of incoming and outgoing particles.
                # e.g. two incoming gluons and four outgoing leptons.
		if mytree.event_type == 2.0:
		    pboost = [[vecHiggs.M()/2., 0., 0., vecHiggs.M()/2.],
                              [vecHiggs.M()/2., 0., 0., -vecHiggs.M()/2.],
                              [lep1Z2.E(), lep1Z2.Px(), lep1Z2.Py(), lep1Z2.Pz()],
                              [lep2Z2.E(), lep2Z2.Px(), lep2Z2.Py(), lep2Z2.Pz()],
                              [lep1Z1.E(), lep1Z1.Px(), lep1Z1.Py(), lep1Z1.Pz()],
                              [lep2Z1.E(), lep2Z1.Px(), lep2Z1.Py(), lep2Z1.Pz()]]
		else:
                    pboost = [[vecHiggs.M()/2., 0., 0., vecHiggs.M()/2.],
                              [vecHiggs.M()/2., 0., 0., -vecHiggs.M()/2.],
                              [lep1Z1.E(), lep1Z1.Px(), lep1Z1.Py(), lep1Z1.Pz()],
                              [lep2Z1.E(), lep2Z1.Px(), lep2Z1.Py(), lep2Z1.Pz()],
                              [lep1Z2.E(), lep1Z2.Px(), lep1Z2.Py(), lep1Z2.Pz()],
                              [lep2Z2.E(), lep2Z2.Px(), lep2Z2.Py(), lep2Z2.Pz()]]

                # Further info on the below can be found in the Madgraph offical website.
                mom = invert_momenta(pboost)
                alphas = 0.118
                nhel = -1 # means sum over all helicity.
                proc_id = -1
                # If you use the syntax "@X" (with X>0), you can set proc_id to that value,
                # (this allows to distinguish process with identical initial/final state.)
                scale2 = 0.0
                
                # The order of particles is important!
		if mytree.event_type == 0.0:
		    pdgs = [21,21,-13,13,-13,13]
		elif mytree.event_type == 1.0:
		    pdgs = [21,21,-11,11,-11,11]
                elif mytree.event_type == 2.0 or mytree.event_type == 3.0:
                    pdgs = [21,21,-11,11,-13,13]

                # Calculation                 
                try:
                    me2 = allmatrix2py.smatrixhel(pdgs,proc_id,mom,alphas,scale2,nhel)
                except:
                    me2 = -999.0
                    pass

                ME_gg4l_value = me2

        # Matrix element calculation can returns a single vaule or an array.
        try:
            ME_SMEFT_gg4l_address[0] = ME_gg4l_value[0]
        except:
            ME_SMEFT_gg4l_address[0] = ME_gg4l_value

        # Print progress.
        if jentry < 10:
            print(ME_gg4l_value)
        if jentry%1000 == 11:
            print('\n' + str(1.0*jentry/total*100) + '% finished')
            print(ME_gg4l_value)
            print(datetime.now()-start)

        newTree.Fill()

    f.Write()
    f.Close()
    print '\n100% finished'
    print(datetime.now()-start)

# Input arguments
op_name = str(sys.argv[1]) # Operator name, e.g. OpgSquared
mc_campaign = str(sys.argv[2]) # e.g. mc16a
truth_or_reco = str(sys.argv[3]) # e.g. truth
sample_name = str(sys.argv[4]) # e.g. mc16_13TeV.507646.MGPy8EG_N30NLOA14_SMEFTNLO_gg4l_OpgSquared_OpgOtp.root

inputdir = ''
outputdir = ''

# Change to your own input and outpur directories.
if truth_or_reco == 'reco':
    inputdir = '/eos/user/w/weiy/ME/minitrees/reco/AntiKt4EMPFlow/'+mc_campaign+'/Nominal/split/'
    outputdir = '/eos/user/w/weiy/ME/minitrees/reco/ME/'+mc_campaign+'/Nominal/split/'

elif truth_or_reco == 'truth':
    inputdir = '/eos/user/w/weiy/ME/minitrees/truth/AntiKt4EMPFlow/'+mc_campaign+'/Nominal/split/'
    outputdir = '/eos/user/w/weiy/ME/minitrees/truth/ME/'+mc_campaign+'/Nominal/split/'

# Calculate the matrix element.
Calculate_ME(inputdir, outputdir, sample_name, op_name)
