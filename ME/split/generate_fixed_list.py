import os
import itertools 
import glob

try:
    from ROOT import TFile, TTree
except:
    print('please source ~/.bashrc_MG.sh')
    exit()

truth_or_reco = 'truth'

if truth_or_reco == 'truth':
    mc_campaigns = ['mc16a'] #truth
else:
    mc_campaigns = ['mc16a','mc16d','mc16e']

#mc_campaigns = ['mc16e'] #test

EFT_ops = ['OpgSM', 'OtpSM', 'OptSM', 'OpgSquared', 'OtpSquared', 'OptSquared', 'OpgOtp']

#EFT_ops = ['OpgSquared']

minitree_split_dir = '/eos/user/w/weiy/ME/minitrees/'+truth_or_reco+'/AntiKt4EMPFlow/'
ME_dir = '/eos/user/w/weiy/ME/minitrees/'+truth_or_reco+'/ME/'


broken_files = []

f_broken = open('broken_files.txt', 'w')

for mc_campaign, EFT_op in itertools.product(mc_campaigns, EFT_ops):

    sample_dir = '/eos/user/w/weiy/ME/minitrees/'+truth_or_reco+'/AntiKt4EMPFlow/'+ mc_campaign + '/Nominal/split/'
    for filename in glob.glob(sample_dir+'*.root'):
        ME_file = filename.replace('AntiKt4EMPFlow', 'ME').replace('.root','_ME_'+EFT_op+'.root')
        print(ME_file)
        try:
            size = os.path.getsize(ME_file)
        except: 
            size = -999

        #print('size: ' + str(size))
        try:
            f_root = TFile(ME_file)
            f_tree = f_root.Get('tree_incl_all')
            entries = f_tree.GetEntriesFast()
        except:
            print('cannot open '+ME_file)
            #if size != 0 :
            broken_files.append(ME_file)
            f_broken.write(ME_file+'\n')
            continue
        #if line != last_line and entries != 10000:
        #    print(line[8]+' entries: '+str(entries))
        #elif line == last_line:
        #    print('last one: ' + line[8]+' entries: '+str(entries))  

        f_root.Close()

print('broken files: ' + str(broken_files))
f_broken.close()

f = open('input_list_fixed.txt', 'w')

if len(broken_files) == 0:
    print('Well done. All EFT samples generated!')
    f.close()
    exit()

for line in broken_files:
    sample = line.split('/')
    for EFT_op in EFT_ops:
        if '_ME_'+EFT_op in line:
            f.write(EFT_op+','+sample[9]+','+truth_or_reco+','+sample[-1].replace('_ME_'+EFT_op, '')+'\n')
f.close()

print('please check broken_files.txt and input_list_fixed.txt to re-submit')
print('done')
