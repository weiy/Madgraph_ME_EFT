import os
import glob
import itertools

mc_campaigns = ['mc16a', 'mc16d', 'mc16e'] #reco
#mc_campaigns = ['mc16a'] #for truth

#EFT_ops = ['OpgSM', 'OtpSM', 'OptSM', 'OpgSquared', 'OtpSquared', 'OptSquared', 'OpgOtp']
EFT_ops = ['OpgSM', 'OpgSquared', 'OtpSquared', 'OptSquared', 'OpgOtp']

input_list = open('input_list.txt', 'w')

for mc_campaign, EFT_op in itertools.product(mc_campaigns, EFT_ops):
    sample_dir = '/eos/user/w/weiy/ME/minitrees/reco/AntiKt4EMPFlow/'+mc_campaign+'/Nominal/split/'#reco
    #sample_dir = '/eos/user/w/weiy/ME/minitrees/truth/AntiKt4EMPFlow/'+mc_campaign+'/Nominal/split/'
            
    for filename in glob.glob(sample_dir+'./*.root'):
        #input_list.write(EFT_op+','+mc_campaign+',truth,'+filename.split('/')[-1]+'\n')
        input_list.write(EFT_op+','+mc_campaign+',reco,'+filename.split('/')[-1]+'\n')

print('done')
