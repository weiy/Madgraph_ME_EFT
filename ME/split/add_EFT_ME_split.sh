#!/bin/bash

source /afs/cern.ch/user/w/weiy/.bashrc_MG.sh

ME_dir=/eos/user/w/weiy/ME/MG5_aMC_v2_9_3/gg4l_$1_standalone_python_2.7.16/SubProcesses

if [ $3 == "truth" ] 
then
    script=add_EFT_ME_split_truth.py
else
    script=add_EFT_ME_split.py
fi

rsync -ah --progress /afs/cern.ch/user/w/weiy/ME/split/${script} ${ME_dir}/

cd ${ME_dir}

python ${script} $1 $2 $3 $4
