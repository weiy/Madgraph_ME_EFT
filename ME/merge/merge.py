import os
import itertools 
import glob

#https://stackoverflow.com/questions/4836710/is-there-a-built-in-function-for-string-natural-sort
try:
    from natsort import natsorted, ns
    from ROOT import TFile, TTree
except:
    print('please source ~/.bashrc_MG.sh')
    exit()

truth_or_reco = 'truth'

if truth_or_reco == 'truth':
    mc_campaigns = ['mc16a'] #truth
    jos = ['345706','corrected']
else:
    mc_campaigns = ['mc16a','mc16d','mc16e']
    jos = ['345706','346899','364250','364252','507646','507648','507650','507652','507654','507656']

EFT_ops = ['OpgSM', 'OtpSM', 'OptSM', 'OpgSquared', 'OtpSquared', 'OptSquared', 'OpgOtp']

minitree_split_dir = '/eos/user/w/weiy/ME/minitrees/'+truth_or_reco+'/AntiKt4EMPFlow/'
ME_dir = '/eos/user/w/weiy/ME/minitrees/'+truth_or_reco+'/ME/'

n = 0
for jo, mc_campaign, EFT_op in itertools.product(jos, mc_campaigns, EFT_ops):

    minitree_sample_dir = minitree_split_dir+ mc_campaign + '/Nominal/split/' 
    #EFT_sample_file = 'samples/' +mc_campaign+'_'+ jo+'_'+EFT_op+'.txt'
    #EFT_sample_split_dir = ME_dir + mc_campaign+ '/Nominal/split/'
    #EFT_sample_merge_dir = ME_dir + mc_campaign+ '/Nominal/merge/'
    
    #os.system('ls -lhv '+sample_split_dir+'*'+jo+'*'+EFT_op+'* > ' + sample_file)
    sorted_minitree_sample = natsorted(glob.glob(minitree_sample_dir+'*'+jo+'*.root'), key=lambda y: y.lower())
    hadd_command = ' '
    for filename in sorted_minitree_sample:
        ME_file = filename.replace('AntiKt4EMPFlow', 'ME').replace('.root','_ME_'+EFT_op+'.root')
        if filename == sorted_minitree_sample[0]:
            ME_file_0 = ME_file
        #print(filename)
        hadd_command += ME_file + ' '
        n+=1

    merged_file = ME_file_0.replace('split','merge').replace('-0','')
    print('merging ' + merged_file + ' in background...')
    hadd_command = 'hadd -f '+merged_file +' '+ hadd_command
    print(hadd_command)
    print('')
    os.system(hadd_command + ' &')
    #print('done')
    #exit()

    '''
    with open(sample_file, 'r') as f:
        lines = f.read().splitlines()
        last_line = lines[-1]

    for line in open(sample_file):
        #print(line)
        line = line.replace('\n','').split()
        #print(line)
        try:
            f_root = TFile(line[8])
            f_tree = f_root.Get('tree_incl_all')
            entries = f_tree.GetEntriesFast()
        except:
            print('cannot open '+line[8])
            broken_files.append(line[8])
            f_broken.write(line[8]+'\n')
            continue
        if line != last_line and entries != 10000:
            print(line[8]+' entries: '+str(entries))
        elif line == last_line:
            print('last one: ' + line[8]+' entries: '+str(entries))            
            
        f_root.Close()
    '''     
#print('broken files: ' + str(broken_files))
#print('please check broken_files.txt')
#check whether 1k events
print('please check background processes by ps')

print(n)
