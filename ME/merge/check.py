import os 
import glob

try:
    from termcolor import colored
    from ROOT import TFile, TTree
except:
     print('please source ~/.bashrc_MG.sh')
     exit()

truth_or_reco = 'truth'
 
if truth_or_reco == 'truth':
    mc_campaigns = ['mc16a'] #truth
else:
    mc_campaigns = ['mc16a','mc16d','mc16e']

EFT_ops = ['OpgSM', 'OtpSM', 'OptSM', 'OpgSquared', 'OtpSquared', 'OptSquared', 'OpgOtp']

minitree_split_dir = '/eos/user/w/weiy/ME/minitrees/'+truth_or_reco+'/AntiKt4EMPFlow/'
ME_dir = '/eos/user/w/weiy/ME/minitrees/'+truth_or_reco+'/ME/'


for mc_campaign in mc_campaigns:
    minitree_sample_dir = minitree_split_dir+ mc_campaign + '/Nominal/'
    for filename in glob.glob(minitree_sample_dir+'*.root'):
        ME_EFT_files = dict()
        ME_EFT_trees = dict()
        for EFT_op in EFT_ops:
            ME_EFT_files[EFT_op] = TFile(filename.replace('AntiKt4EMPFlow', 'ME').replace('Nominal', 'Nominal/merge').replace('.root','_ME_'+EFT_op+'.root'))
            #print(ME_EFT_files[EFT_op])
            ME_EFT_trees[EFT_op] = ME_EFT_files[EFT_op].Get('tree_incl_all')
            #print(ME_EFT_trees[EFT_op])
        
        f_SM = TFile(filename)
        t_SM = f_SM.Get('tree_incl_all')

        print('checking ' + filename)
        print('1. checking number of events: ')

        for EFT_op in EFT_ops:
            if  t_SM.GetEntries() != ME_EFT_trees[EFT_op].GetEntries():
                print(colored('number of events are different. Exit.', 'red'))
                exit()
        print(colored('number of events are the same. They are ' + str(t_SM.GetEntries()) + '. Great!','yellow'))

        print('')
        
        if truth_or_reco == 'truth':
            print('2. checking higgs_m_fidDres_truth lists...')
        else:
            print('2. checking m4l_fsr lists...')

        for jentry in range(0, t_SM.GetEntries()):
            t_SM.GetEntry(jentry)
            for EFT_op in EFT_ops:
                ME_EFT_trees[EFT_op].GetEntry(jentry)
            if jentry%30000==0:
                print(str(1.0*jentry/t_SM.GetEntries()*100) + '% finished')
                
            for EFT_op in EFT_ops:
                if truth_or_reco == 'truth':
                    if t_SM.higgs_m_fidDres_truth !=  ME_EFT_trees[EFT_op].higgs_m_fidDres_truth:
                        print(colored('higgs_m_fidDres_truth are different. Exit','red'))
                        exit()
                else:                
                    if t_SM.m4l_fsr !=  ME_EFT_trees[EFT_op].m4l_fsr:
                        print(colored('m4ls are different. Exit','red'))
                        exit()
        if truth_or_reco == 'truth':
            print(colored('higgs_m_fidDres_truth are the same. Great!','yellow'))
        else:
            print(colored('m4ls are the same. Great!','yellow'))
        f_SM.Close()
        for EFT_op in EFT_ops:
            ME_EFT_files[EFT_op].Close()
        print('\n\n')
