# Install root on centos 7.9

1. install root

    - Official link: [https://root.cern/install/build_from_source/](https://root.cern/install/build_from_source/)
	
	- on lxplus: go to /tmp (faster) to download src and build or eos is also ok

    - `cd /home/weiy/Software/src/root`
	
	- `git clone https://github.com/root-project/root.git .`

    - `git branch` to see current branch, and `git tag` to see tags available.

    - `git checkout v6-18-04`

	- build command
	
		- `cd ${HOME}/Software/build/root/root-6.18.04` or the proper version 

	    - `cmake /tmp/weiy/weiy/Software/src/root -DCMAKE_INSTALL_PREFIX=/eos/user/w/weiy/Software/install/root/root-6.18.04 -DCMAKE_CXX_STANDARD=14 -DCMAKE_C_COMPILER=/cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/bin/gcc -DCMAKE_CXX_COMPILER=/cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/bin/g++ -DPython2_ROOT_DIR=/eos/user/w/weiy/Software/install/python/python-2.7.16/bin/python -Dimt=ON -Dbuiltin_fftw3=ON -Ddavix=OFF -Dbuiltin_xrootd=ON -Dpythia8=OFF -Dbuiltin_lz4=ON -Dbuiltin_zlib=ON -Dbuiltin_tbb=ON -Dbuiltin_vdt=ON -Dbuiltin_lzma=ON -Dbuiltin_xxhash=ON -Dbuiltin_gsl=ON -Dbuiltin_opengl=ON -Dbuiltin_x11=ON`
	
        - `cmake --build . -- -j20`, where 4 is the number of cores of your cpu avaiable or you want

        - `make install` : don't forget this before run root
	
	    - set the root path in __.bashrc_MG__
