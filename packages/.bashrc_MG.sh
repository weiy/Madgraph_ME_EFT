INSTALL=/eos/user/w/weiy/Software/install/

#gcc-8.3.0
source /cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/setup.sh

#cmake-3.14.3
export PATH=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.14.3/Linux-x86_64/bin:$PATH

#python-2.7.16
PY_VR_SELF=python-2.7.16 #for mdgraph
if [ $PY_VR_SELF ]; then 
  export PATH=${INSTALL}/python/$PY_VR_SELF/bin:/afs/cern.ch/user/w/weiy/.local/bin:$PATH
  export PYTHONPATH=${INSTALL}/$PY_VR_SELF/bin:${INSTALL}/python/$PY_VR_SELF/lib/python2.7/site-packages:/afs/cern.ch/user/w/weiy/.local/lib/python2.7/site-packages:$PYTHONPATH
  export LD_LIBRARY_PATH=${INSTALL}/python/$PY_VR_SELF/lib:/afs/cern.ch/user/w/weiy/.local/lib/python2.7/site-packages:$LD_LIBRARY_PATH
fi


#root
ROOT_VR_SELF=root-6.18.04

if [ $ROOT_VR_SELF ]; then 
    export ROOTSYS=${INSTALL}/root/$ROOT_VR_SELF/
else
    unset ROOTSYS
fi

if [ $ROOTSYS ]; then
    export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH
    export PATH=$ROOTSYS/bin:$PATH
    source $ROOTSYS/bin/thisroot.sh    
fi

alias root='root -l'
