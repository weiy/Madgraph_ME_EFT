# How to install python in centos 7.9

1. install

    - remember to source correct gcc version: see 
	
	- on pplxint, only need: 
	
		- `module load gcc/9.2.0`
		
		- or `source /cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/bin/gcc/setup.sh`
		
		- you can find all gcc here: __/cvmfs/sft.cern.ch/lcg/releases/gcc/__
	
	- on lxplus, go to afs (much faster and reliable) or /tmp dir to build and install it into eos.

	- `cd /home/weiy/Software/src/python`
	
    - `git clone https://github.com/python/cpython.git .`
    
    - `git branch`: to see current branch

    - `git tag`: to see all tags available

    - `git checkout v3.6.8`

	- `cd /home/weiy/Software/build/python`
	
	- `mkdir python-3.6.8`
	
	- `cd python-3.6.8`
	
	- `sudo mkdir /home/weiy/Software/install/python/python-3.6.8`
	
    - `/home/weiy/Software/src/python/configure CC=/home/weiy/Software/install/gcc/gcc-8.3.0/bin/gcc CXX=/home/weiy/Software/install/gcc/gcc-8.3.0/bin/g++ --prefix=/home/weiy/Software/install/python/python-3.6.8 --enable-shared --enable-optimizations --enable-loadable-sqlite-extensions 2>&1 | tee configure_self.log `

	- `make -j10 2>&1 | tee make_self.log` about 15 minutes
	
	- `sudo make install 2>&1 | tee install_self.log `
	
	- `cat install_self.log`
	
	- change __$PATH__ in `~/.bashrc_MG.sh` by adding `PY_VR_SELF=python-3.6.8`
	
	- change the ownship of site-packages: `sudo chown -R $USER /home/weiy/Software/install/python/python-3.6.8/lib/site-packages` 

1. setup 

    1. save python history

		- on lxplus without pip: `python2.7 -m ensurepip --default-pip --user`: which will download package under: ${HOME}/.local/
		
		- and `python -m pip install numpy --user` and `python -m pip install gnureadline --user`
		
        - for python2: `pip install` should be changed to: `python -m pip install __"package"__`

        - `pip3 install gnureadline` 

            ```
            pip3 install readline will break python3
            ```
        - write a python start up script
            
            ```
            emacs ~/.pystartup
            see more in detail
            ```
        - configure __~/.bashrc_MG.sh__

            ```
            export PYTHONSTARTUP=$HOME/.pystartup
            ```
		<br>
		
	1. `pip install pyqt5` to solve: Matplotlib is currently using agg, which is a non-GUI backend, so cannot show the figure.
