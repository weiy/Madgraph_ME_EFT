# A tutorial on Madgraph matrix element calculation

[TOC]

## Introduction

The matrix-element based methods have been used for a long time, especially in the $`H{\rightarrow}ZZ{\rightarrow4l}`$ channel. This method takes advantage of majority part of the information of the process, comparing with using only the m4l variable. In this tutorial, we will use Madgraph to calculate the matrix element. Of course, there are some other package providing how to calculate that like MCFM (which is used in the Standard Model analysis in $`H{\rightarrow}ZZ{\rightarrow4l}`$ at ATLAS). 

## Setting up

Five crucial packages are acquired in oder to calculate the matrix-element in this tutorial. 

1. gcc: 8.3.0

1. cmake: 3.14.3

1. python: 2.7.16

1. root: 6.18.04

1. Magraph: 2.9.3

__Some clues to install these software.__ 

1. gcc-v8.3.0: 

    - Use cvmfs gcc: `source /cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/setup.sh`
    
    - I don't recommend to install gcc on lxplus as there are some other required libraries needed to installed. Without root privilege, it's painful to install those.
       
1. cmake 3.14.3

    - no need to install: `export PATH=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.14.3/Linux-x86_64/bin:$PATH `

    <br>

1. python-v2.7.16:

    - As there is no python-v2.7.16 compiled by gcc-v8.3.0 available under cvmfs, we have to install it. 

    - python git package can be found here: [https://github.com/python/cpython.git](https://github.com/python/cpython.git)

    - A general installation has been provided here: [packages/python/README.md](packages/python/README.md). You have to change it to the right version and source the right bashrc script before using this python package. Please email to yingjie.wei@cern.ch if you have any questions.

    - install f2py: `python -m pip install numpy`

1. root-v6.18.04:

    - Root installation should be compiled by gcc-v8.3.0 and link to python-v2.7.16

    - We can find a generall installation instruction here: [packages/root/README.md](packages/root/README.md) 

1. Madgraph-v.2.9.3

    - We can download it here: [https://launchpad.net/mg5amcnlo/2.0/2.9.x/+download/MG5_aMC_v2.9.3.tar.gz](https://launchpad.net/mg5amcnlo/2.0/2.9.x/+download/MG5_aMC_v2.9.3.tar.gz)
    

Package source: before use Madgraph, we should source the packages built by `source .bashrc_MG.sh`. We can have a reference here [packages/.bashrc_MG.sh](packages/.bashrc_MG.sh) 

## Generate the matrix-element standalone library

1. The general information on how to generate the matrix-element standalone library can be found:

    - https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/MadLoopStandaloneLibrary

    - https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/FAQ-General-4

1. Here as an example, we generate the ___OpgSquared___ matrix element in the $`H{\rightarrow}ZZ{\rightarrow4l}`$ channel.

    - Remember source the packages we build: `source .bashrc_MG.sh`

    - Go to Madgraph dir: `cd OUR_MADGRAPH_DIR`

    - Use other packages like PDF in cvmfs: `cp /cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/MCGenerators/madgraph5amc/2.9.3.atlas-a5843/x86_64-centos7-gcc62-opt/input/mg5_configuration.txt ./input/`

    - `python ./bin/madgraph`

    - Type the following commands:

        ```
        set max_npoint_for_channel 4
        import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/SMEFTatNLO-cpG_ctp_cpt
        define p = g u c d s b u~ c~ d~ s~ b~
        define j = g u c d s b u~ c~ d~ s~ b~
        define l+ = e+ mu+
        define l- = e- mu-
        generate g g > l+ l- l+ l- /g QCD=4 QED=4 NP^2==4
        output standalone gg4l_OpgSquared_standalone_python_2.7.16 --prefix=int

        launch -f

        exit
        ```
    
    - `cd OUR_MADGRAPH_DIR/gg4l_OpgSquared_standalone_python_2.7.16/SubProcesses`

    - `make allmatrix2py.so`

    Up to now, the matrix element standalone library has been generated.

1. Please have a reference for all operators like: __OpgSM__, __OtpSM__, __OptSM__, __OpgSquared__, __OtpSquared__, __OptSquared__, __OpgOtp__, __etc__ and how to fix bugs here: [other_operators.txt](other_operators.txt)


## Use the matrix element library to calculate the matrix element 

We can find a python script here: [ME/split/add_EFT_ME_split.py](ME/split/add_EFT_ME_split.py) to take a look on how to use the matrix element standalone library and the input minitrees to calculate the matrix element.

## Cluster submisson

The matrix element calculation is CPU-intensive. The speed of matrix element calculation for operator ___OtpSM___ is ~3k/h. In general, we would have 1M raw events. It's necessary to submit the matrix element calculation on cluster. 

The scripts have been placed here, we can take a look at the __command.txt__ file in each folder: 
    
- [ME/split](ME/split): to calculate the matrix element on cluster with split already minitrees (using split-root: https://go-hep.org/dist/latest/root-split-linux_amd64.exe) 

- and then merge the output: [ME/merge](ME/merge)
